#include <stdio.h>
#include <stdlib.h>

struct node{
    int data;
    struct node *next;
};

struct linked_list{
    struct node *head;
};

struct node* new_node(int data){
    struct node *a = malloc(sizeof(*a));
    a->data = data;
    a->next = NULL;
    
    return a;
}

struct linked_list* new_list(int data){
    struct linked_list *l = malloc(sizeof(*l));
    struct node *a;
    a = new_node(data);
    
    l->head = a;
    
    return l;
}

void transverse(struct linked_list *l){
    struct node *temp = l->head;
    while(temp!=NULL){
        printf("%d\t", temp->data);
        temp = temp->next;
    }
    printf("\n");
}

void at_beginning(struct linked_list *l, struct node *n){
    n->next = l->head;
    l->head = n;
}

void at_end(struct linked_list *l, struct node *n){
    struct node *temp = l->head;
    while(temp->next!=NULL){
        temp = temp->next;
    }
    temp->next = n;
}

void insert_after(struct node *b, struct node *a){
    a->next = b->next;
    b->next = a;
}

int main(){
    struct node *a,*b,*c;
    struct linked_list *l = new_list(1);
    a = new_node(10);
    b = new_node(20);
    c = new_node(30);
    
    l->head->next = a;
    a->next = b;
    b->next = c;
    c->next = NULL;
    
    transverse(l);
    
    struct node *m;
    m = new_node(40);
    insert_after(b,m);
    
    struct node *d;
    d = new_node(60);
    at_beginning(l,d);
    
    struct node *A;
    A = new_node(-4);
    at_end(l,A);
    
    transverse(l);
    return 0;
}