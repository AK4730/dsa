#include<stdio.h>
#include<stdlib.h>

typedef struct circular_node{
    int data;
    struct circular_node *next;
}cn;

typedef struct circular_list{
    cn *last;
}cl;

cn* create_node(int data){
    cn *n = malloc(sizeof(*n));
    n->data = data;
    n->next = n;
    return n;
}

cl* create_clist(int data){
    cl *list = malloc(sizeof(*list));
    cn *n = create_node(data);
    list->last = n;
    return list;
}

void add_node(cl *list, int data){
    cn *temp = list->last;
    cn *n = create_node(data);
    n->next = temp->next;
    temp->next = n;
    list->last = n;
}

void print(cl *list){
    cn *temp = list ->last->next;
    do{
        printf("%d ",temp->data);
        temp = temp->next;
    }while(temp!=list->last->next);
    printf("\n");
}

int main(){
    cl *list = create_clist(40);
    cl *list2 = create_clist(99);
    
    add_node(list,60);
    add_node(list,90);
    add_node(list,50);
    
    add_node(list2,67);
    add_node(list2,55);
    print(list);
    print(list2);
}