#include <stdio.h>
#include <stdlib.h> //dont

typedef struct Node{
    int data;
    struct Node *next;
    struct Node *prev;
}node;

typedef struct linked_list{
    node *head;
    node *tail;
}ll;

node* create_node(int data){
    node *n = malloc(sizeof(*n));
    n->data = data;
    n->next = NULL;
    n->prev = NULL;
    return n;
}

ll* create_list(int data){
   ll *list = malloc(sizeof(*list));
   node *n = create_node(data);
   list->head = n;
   node *temp = list->head;
   while(temp->next!=NULL)
        temp = temp->next;
    
    list->tail = temp;
    return list;
}

void add_begining(ll *list, int data){
    node *n = create_node(data);
    n->next = list->head;
    list->head->prev = n;
    list->head = n;
}

void add_node(ll *list , int data){
    node *temp = list->head;
    node *n = create_node(data);
    while(temp->next!=NULL)
        temp = temp->next;
    temp->next = n;
    n->prev = temp;
    list->tail = n;
    /*Forgot the tail*/
}

void add_node_at(ll *l,int data, int position){
    node *temp = l->head;
    node *n = create_node(data);
    if(position==0){
        temp->prev = n;
        n->next = temp;
        l->head = n;
    }else{
        position--;
        while(position--){
            temp = temp->next;
        }
        n->next = temp->next;
        n->prev = temp;
        temp->next->prev = n;
        temp->next = n;
        /* Need to pay attention here and extra care needed */
    }
}

void print(ll *list, int x){
    if(x){
        node *temp = list->tail;
        while(temp){
            printf("%d ",temp->data);
            temp = temp->prev;
        }
    }else{
        node *temp = list->head;
        while(temp){
            printf("%d ",temp->data);
            temp = temp->next;
        }
    }
    printf("\n");
}

void free_list(ll *list){
    node *temp = list->head;
    while(temp){
        free(temp);
    }
}

int main(){
    
    int num_node,temp;
    printf("Enter number of nodes\n");
    scanf("%d",&num_node);
    
    printf("Enter your node data\n");
    scanf("%d",&temp);
    ll *list = create_list(temp);
    
    for(int i = 1;i<num_node;i++){
        scanf("%d",&temp);
        add_node(list,temp);
    }
    
    print(list,0);
    print(list,1);
    
    add_begining(list,90);
    add_node_at(list,50,2);
    
    //free_list(list);
    
    print(list,0);
    print(list,1);
}